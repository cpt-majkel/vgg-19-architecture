# CNN Architectures 

This repository contains architectures based on VGG-19, VGG-16, Inception and Xception neural networks. 
Is is for testing purposes.

### Prerequisites

Architecture was developed in Python3.6 

Following packages were used:
- [Keras 2.2.4](https://keras.io/#installation)
- [Tensorflow - GPU](https://www.tensorflow.org/install/gpu) 1.13.1 as backend engine
- [imbalanced-learn](https://imbalanced-learn.readthedocs.io/en/stable/install.html) 0.4.3

## Usage
At the moment there are separate filer per architecture. I.e ```vgg.py``` uses prepared data in pickle and runs the model. 
```prepare_data.py``` creates pickle files from data (labeling has to be done manually).

## Author
**Michał Piekarski** - [cpt-majkel](https://gitlab.com/cpt-majkel)
