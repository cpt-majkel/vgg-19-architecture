import pickle
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from imblearn.over_sampling import SMOTE
import numpy as np
import pandas as pd


def main():
    with open('data/X.pkl', 'rb') as f:
        X = pickle.load(f)
    with open('data/Y.pkl', 'rb') as f:
        Y = pickle.load(f)
    with open('data/X_t.pkl', 'rb') as f:
        X_test = pickle.load(f)
    with open('data/Y_t.pkl', 'rb') as f:
        Y_test = pickle.load(f)

    X_train, X_valid, Y_train, Y_valid = train_test_split(X, Y, test_size=0.2, random_state=1, shuffle=True)
    X_train_res, y_train_res = balance_data(X_train, Y_train)
    x_test_res, y_test_res = balance_data(X_test, Y_test)
    x_valid_res, y_valid_res = balance_data(X_valid, Y_valid)

    file_Xtr = open('data/X_train.pkl', 'wb')
    file_Xval = open('data/X_valid.pkl', 'wb')
    file_Ytr = open('data/Y_train.pkl', 'wb')
    file_Yva = open('data/Y_valid.pkl', 'wb')
    file_Xte = open('data/X_test.pkl', 'wb')
    file_Yte = open('data/Y_test.pkl', 'wb')
    pickle.dump(X_train_res, file_Xtr)
    pickle.dump(y_train_res, file_Ytr)
    pickle.dump(x_test_res, file_Xte)
    pickle.dump(y_test_res, file_Yte)
    pickle.dump(x_valid_res, file_Xval)
    pickle.dump(y_valid_res, file_Yva)


def balance_data(X, y):
    sm = SMOTE()
    flattened = []
    for image in X:
        x1 = image[:, :, 0]
        flattened.append(x1.flatten())
    flat = np.array(flattened)
    X_res, y_res = sm.fit_sample(flat, y)
    X_final = []
    for res in X_res:
        reshaped = np.reshape(res, (64, 64))
        X_final.append(np.stack((reshaped,) * 3, axis=-1))
    X_final = np.array(X_final)
    y_res = pd.get_dummies(y_res)
    return X_final, y_res


if __name__ == "__main__":
    main()
