import pandas as pd
import pickle
import numpy as np


def main():
    n = 64
    readings = ['data/finalRes/pumps_r1_12_02_15_02.csv', 'data/finalRes/pumps_r1_19_02_22_02.csv',
                'data/finalRes/pumps_r1_26_02_01_03.csv', 'data/finalRes/pumps_r1_05_03_08_03.csv']
    labels = ['data/labels/pumps_r1_12_02_15_02_lbl.csv', 'data/labels/pumps_r1_19_02_22_02_lbl.csv',
              'data/labels/pumps_r1_26_02_01_03_lbl.csv', 'data/labels/pumps_r1_05_03_08_03_lbl.csv']
    test_read = ['data/finalRes/pumps_r1_26_03_28_03_test.csv']
    test_label = ['data/labels/pumps_r1_26_03_28_03_test_lbl.csv']
    X = []
    Y = []
    #
    # for file, lbl in zip(readings, labels):
    #     data = pd.read_csv(file, sep=None, header=0)
    #     label = pd.read_csv(lbl, sep=None, header=0, usecols=[0])
    #     data = data.drop(['time', 'date'], axis=1)
    #     data.interpolate()
    #     for col in data.columns:
    #         # data[col] = (data[col] - data[col].mean()) / data[col].std()
    #         data[col].fillna(0, inplace=True)
    #     data_sp = [data[i:i+n].values for i in range(0, data.shape[0], n)]
    #     data_sp.pop(-1)
    #     for arr in data_sp:
    #         X.append(np.stack((arr,)*3, axis=-1))
    #     Y.extend(label.values)
    # Y_scalar = [float(x) for x in Y]

    X_test_val = []
    Y_tet = []
    for test_dat, test_lbl in zip(test_read, test_label):
        test_data = pd.read_csv(test_dat, sep=None, header=0)
        #label = pd.read_csv(test_lbl, sep=None, header=0, usecols=[0])
        #test_data = test_data.drop(['time', 'date'], axis=1)
        #for col in test_data.columns:
            # test_data[col] = (test_data[col] - test_data[col].mean()) / test_data[col].std()
        #    test_data[col].fillna(0, inplace=True)
        test_data_sp = [test_data[i:i + n].values for i in range(0, test_data.shape[0], n)]
        for arr in test_data_sp:
            arr.plot()
            input("Press.. ")
            #X_test_val.append(np.stack((arr,) * 3, axis=-1))
        #Y_tet.extend(label.values)
    #Y_test_sc = [float(x) for x in Y_tet]

    #X_test_val.pop(-1)

    #Y_test = np.array(Y_test_sc)
    #X_test = np.array(X_test_val)
    # X_values = np.array(X)
    # Y_values = np.array(Y_scalar)
    # file_X = open('data/X_raw.pkl', 'wb')
    # file_Y = open('data/Y_raw.pkl', 'wb')
    # pickle.dump(X_values, file_X)
    # pickle.dump(Y_values, file_Y)
    #file_X_test = open('data/X_t_raw.pkl', 'wb')
    #file_Y_test = open('data/Y_t_raw.pkl', 'wb')
    #pickle.dump(X_test, file_X_test)
    #pickle.dump(Y_test, file_Y_test)


if __name__ == "__main__":
    main()
