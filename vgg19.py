# Author: Michal Piekarski

import os
import pickle
import time

import keras
import numpy as np
import pandas as pd
from keras.applications.vgg19 import VGG19, preprocess_input
from keras.layers import Dense, Dropout, Flatten
from keras.models import Model, Sequential
from sklearn.metrics import confusion_matrix
from tensorflow import set_random_seed

import psutil

set_random_seed(1)
np.random.seed(1)


def main():
    with open("data/X_train.pkl", "rb") as f:
        X_train_res = pickle.load(f)
    with open("data/Y_train.pkl", "rb") as f:
        y_train_res = pickle.load(f)
    with open("data/X_valid.pkl", "rb") as f:
        x_valid_res = pickle.load(f)
    with open("data/Y_valid.pkl", "rb") as f:
        y_valid_res = pickle.load(f)
    with open("data/X_test.pkl", "rb") as f:
        x_test_res = pickle.load(f)
    with open("data/Y_test.pkl", "rb") as f:
        y_test_res = pickle.load(f)

    for x in X_train_res:
        preprocess_input(x)

    for x in x_test_res:
        preprocess_input(x)

    for x in x_valid_res:
        preprocess_input(x)

    pid = os.getpid()
    py = psutil.Process(pid)
    memoryUse1 = py.memory_info()[0] / 2.0 ** 30  # memory use in GB

    base_model = VGG19(
        weights="imagenet", include_top=False, input_shape=(64, 64, 3), classes=2
    )

    top_model = Sequential()
    top_model.add(Dense(500, activation="relu"))
    top_model.add(Dropout(0.5))
    top_model.add(Dense(2, activation="softmax"))

    x = base_model.output
    x = Flatten()(x)
    predictions = top_model(x)

    model = Model(inputs=base_model.input, outputs=predictions)

    for layer in base_model.layers:
        layer.trainable = False

    model.compile(
        loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"]
    )

    callbacks_list = [
        keras.callbacks.EarlyStopping(monitor="val_acc", patience=3, verbose=1)
    ]
    model.summary()

    start_time = time.time()
    history = model.fit(
        X_train_res,
        y_train_res,
        epochs=10,
        validation_data=(x_valid_res, y_valid_res),
        verbose=1,
    )

    memoryUse2 = py.memory_info()[0] / 2.0 ** 30
    CPUUse = psutil.cpu_percent()

    scores = model.evaluate(x_test_res, pd.get_dummies(y_test_res), verbose=1)
    stop_time = time.time()
    print("%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))

    predictions = model.predict(x_test_res)
    y_pred = predictions > 0.5
    # file_Y = open('vgg19_hist', 'wb')
    # pickle.dump(history, file_Y)

    print(f"Resources use:{memoryUse1, memoryUse2, CPUUse}")
    print(f"Elapsed time:{stop_time - start_time}")
    matrix = confusion_matrix(
        np.array(y_test_res).argmax(axis=1), y_pred.argmax(axis=1)
    )
    print("\nConfusion matrix:\n", matrix)


if __name__ == "__main__":
    main()
